<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_model extends CI_Model{
	private $_table = "barang";

	public function TampilDataBarang()
	{
	return $this->db->get($this->_table)->result();
	}
	
	public function TampilDataBarang2()
	{
	$query = $this->db->query("select * from jenis_barang as jb inner join barang as b on jb.kode_jenis=b.kode_jenis");
	return $query->result();
	}

	public function TampilDataBarang3()
	{
	$this->db->select('*');
	$this->db->order_by('kode_barang','ASC');
	$result = $this->db->get($this->_table);
	return $result->result();
	}
	public function  save()
	{
	$data['kode_barang']	= $this->input->post('kode_barang');
	$data['nama_barang']	= $this->input->post('nama_barang');
	$data['harga_barang']	= $this->input->post('harga_barang');
	$data['kode_jenis']		= $this->input->post('kode_jenis');
	$data['stok']			= $this->input->post('stok');
	$data['flag']			= 1;
	$this->db->insert($this->_table, $data);
	}
	
	public function detail($kode_barang)
    {
        $query	= $this->db->query("SELECT barang.*, jenis_barang.nama_jenis FROM barang INNER JOIN  jenis_barang ON barang.kode_jenis = jenis_barang.kode_jenis WHERE barang.flag = 1 AND barang.kode_barang = '$kode_barang'");
        return $query->result();
    }

	public function edit($kode_barang)
	{
		$this->db->select('*');
		$this->db->where('kode_barang', $kode_barang);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function update($kode_barang)
	{
	$data['kode_barang']	= $this->input->post('kode_barang');
	$data['nama_barang']	= $this->input->post('nama_barang');
	$data['harga_barang']	= $this->input->post('harga_barang');
	$data['kode_jenis']		= $this->input->post('kode_jenis');
	$data['flag']			= 1;
	$this->db->where('kode_barang', $kode_barang);
	$this->db->update($this->_table, $data);
	}
	public function delete($kode_barang)
	{
		$this->db->where('kode_barang', $kode_barang);
		$this->db->delete($this->_table);
	}
	 public function updateStok($kode_barang, $qty)
    {
        //panggil data detail stok
        $cari_stok = $this->detail($kode_barang);
        foreach ($cari_stok as $data) {
            $stok = $data->stok;
        }
        
        //proses update stok table barang
        $jumlah_stok     = $stok + $qty;
        $data_barang['stok']    = $jumlah_stok;
        
        $this->db->where('kode_barang', $kode_barang);
        $this->db->update('barang', $data_barang);
    }

}