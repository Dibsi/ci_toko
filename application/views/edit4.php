<?php
	foreach ($detail_supplier as $data) {
		$kode_supplier	= $data->kode_supplier;
		$nama_supplier	= $data->nama_supplier;
		$alamat			= $data->alamat;
		$telp			= $data->telp;
	}
?>
<div align="center"><h1>Edit Data Supplier</h1></div>
<form method="POST" action="<?=base_url()?>supplier/EditSupplier/<?= $kode_supplier; ?>">
<table width="50%" border="0" cellpadding="5" bgcolor="#00CC66" align="center">
  <tr>
    <td>Kode Supplier</td>
    <td>:</td>
    <td><input value="<?= $kode_supplier; ?>" type="text" name="kode_supplier" id="kode_supplier" maxlength="10"></td>
  </tr>
  <tr>
    <td>Nama Supplier</td>
    <td>:</td>
    <td><input value="<?= $nama_supplier; ?>" type="text" name="nama_supplier" id="nama_supplier" maxlength="50"></td>
  </tr>
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input value="<?= $telp; ?>" type="text" name="telp" id="telp" maxlength="50"></td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea  value=<?= $alamat; ?> name="alamat" id="alamat" cols="45" rows="5" ><?=$alamat;?></textarea></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="submit" id="submit" value="Simpan">
      <input type="reset" name="reset" id="reset" value="Reset"></td>
  </tr>
  <td>&nbsp;</td>
    <td>&nbsp;</td>
   <td width="334"><a href="<?=base_url();?>supplier/listsupplier"><input type="button" name="button" id="button" value="Kembali Ke Menu Sebelumnya"></td>
   </td>
</table>
</form>