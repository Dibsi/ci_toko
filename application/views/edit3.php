<?php
	foreach ($detail_barang as $data) {
		$kode_barang	= $data->kode_barang;
		$nama_barang	= $data->nama_barang;
		$harga_barang	= $data->harga_barang;
		$kode_jenis		= $data->kode_jenis;
	}
?>
<div align="center"><h1>Edit Data Barang</h1></div>
<form method="POST" action="<?=base_url()?>barang/EditBarang/<?= $kode_barang; ?>">
<table width="50%" border="0" cellpadding="5" bgcolor="#00CC66" align="center">
  <tr>
    <td>Kode barang</td>
    <td>:</td>
    <td><input value="<?= $kode_barang; ?>" type="text" name="kode_barang" id="kode_barang" maxlength="10"></td>
  </tr>
  <tr>
    <td>Nama Barang</td>
    <td>:</td>
    <td><input value="<?= $nama_barang; ?>" type="text" name="nama_barang" id="nama_barang" maxlength="50"></td>
  </tr>
  <tr>
    <td>Harga Barang</td>
    <td>:</td>
    <td><input value="<?= $harga_barang; ?>" type="text" name="harga_barang" id="harga_barang" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Jenis Barang</td>
    <td>:</td>
    <td><select name="kode_jenis" id="kode_jenis">
  <?php
  foreach ($data_jenis as $data){ 
  	 $select_jenis_barang = ($data->kode_jenis ==
	 $kode_jenis) ? 'selected' : '';
	 ?>
	 <option value="<?= $data->kode_jenis; ?>">
     <?= $data->kode_jenis; ?> |
      <?= $data->nama_jenis; ?> 
     </option>
  <?php } ?>
    </select>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="submit" id="submit" value="Simpan">
      <input type="reset" name="reset" id="reset" value="Reset"></td>
  </tr>
  <td>&nbsp;</td>
    <td>&nbsp;</td>
   <td width="334"><a href="<?=base_url();?>barang/listbarang"><input type="button" name="button" id="button" value="Kembali Ke Menu Sebelumnya"></td>
   </td>
</table>
</form>