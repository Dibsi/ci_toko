<div align="center"><h1>Input Data Karyawan</h1></div>
<form method="POST" action="<?=base_url()?>karyawan/InputKaryawan">
<table width="50%" border="0" cellpadding="5" bgcolor="#00CC66" align="center">
  <tr>
    <td>NIK</td>
    <td>:</td>
    <td><input type="text" name="nik" id="nik" maxlength="10"></td>
  </tr>
  <tr>
    <td>Nama Karyawan</td>
    <td>:</td>
    <td><input type="text" name="nama_karyawan" id="nama_karyawan" maxlength="50"></td>
  </tr>
  <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td><input type="text" name="tempat_lahir" id="tempat_lahir" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Jenis Kelamin</td>
    <td>:</td>
    <td><select name="jenis_kelamin" id="jenis_kelamin">
    	<option value="L">Laki-laki</option>
        <option value="P">Perempuan</option>
    </select>
    </td>
  </tr>
  <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td><select name="tgl" id="tgl">
    	<?php
        for($tgl=0;$tgl<=31;$tgl++){
		if($tgl == date('d')){
			$slc = 'SELECTED';
		}else{
			$slc = '';
		}
		?>
        <option <?=$slc;?> value="<?=$tgl;?>"><?=$tgl;?></option>
        <?php
        }
		?>
    </select>
      
      <select name="bln" id="bln">
      <?php
      	$bulan = array ('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
		$b=0;
		while(each($bulan)){
			if($b+1== date('m')){
				$n = 'SELECTED';
			}else{
				$n = '';
			}		
	  ?>
      <option <?=$n;?> value="<?=$b+1;?>" ><?=$bulan[$b];?></option>
      <?php
      	$b++;
		}
	  ?>
      </select>
      <select name="thn" id="thn">
      <?php
		for($t=date('Y')-60;$t<= date('Y')-15;$t++){
					
		?>
		<option value="<?=$t;?>"><?=$t;?></option>
					
		<?php
		}
		?>
      </select>
      </td>
  </tr>
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input type="text" name="telp" id="telp" maxlength="50"></td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5" ></textarea></td>
  </tr>
  <tr>
    <td>Jabatan</td>
    <td>:</td>
    <td><select name="kode_jabatan" id="kode_jabatan">
  <?php
  foreach ($data_jabatan as $data){ ?>
	 <option value="<?= $data->kode_jabatan; ?>">
     <?= $data->nama_jabatan; ?></option>
  <?php } ?>
    </select>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="submit" id="submit" value="Simpan">
      <input type="reset" name="reset" id="reset" value="Reset"></td>
  </tr>
  <td>&nbsp;</td>
    <td>&nbsp;</td>
   <td width="334"><a href="<?=base_url();?>karyawan/listkaryawan"><input type="button" name="button" id="button" value="Kembali Ke Menu Sebelumnya"></td>
   </td>
</table>
</form>