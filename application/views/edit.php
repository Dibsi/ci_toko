<?php
	foreach ($detail_karyawan as $data) {
		$nik			= $data->nik;
		$nama_lengkap	= $data->nama_lengkap;
		$tempat_lahir	= $data->tempat_lahir;
		$tgl_lahir		= $data->tgl_lahir;
		$jenis_kelamin  = $data->jenis_kelamin;
		$alamat			= $data->alamat;
		$telp			= $data->telp;
		$kode_jabatan	= $data->kode_jabatan;
	}
	$thn_pisah = substr($tgl_lahir, 0, 4);
	$bln_pisah = substr($tgl_lahir, 5, 2);
	$tgl_pisah = substr($tgl_lahir, 8, 2);
?>
<div align="center"><h1>Edit Data Karyawan</h1></div>
<form method="POST" action="<?=base_url()?>karyawan/EditKaryawan/<?= $nik; ?>">
<table width="50%" border="0" cellpadding="5" bgcolor="#00CC66" align="center">
  <tr>
    <td>NIK</td>
    <td>:</td>
    <td><input value="<?= $nik; ?>" type="text" name="nik" id="nik" maxlength="10" readonly></td>
  </tr>
  <tr>
    <td>Nama Karyawan</td>
    <td>:</td>
    <td><input value="<?= $nama_lengkap; ?>" type="text" name="nama_karyawan" id="nama_karyawan" maxlength="50"></td>
  </tr>
  <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td><input value="<?= $tempat_lahir; ?>" type="text" name="tempat_lahir" id="tempat_lahir" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Jenis Kelamin</td>
    <td>:</td>
    <td>
    <?php
    	if ($jenis_kelamin == 'L'){
			$slc_p = '';
			$slc_l = 'selected';
		}elseif ($jenis_kelamin == 'P'){
			$slc_l = '';
			$slc_p = 'selected';
		}else{
			$slc_p = '';
			$slc_l = '';	
		}
	?>	
    <select name="jenis_kelamin" id="jenis_kelamin">
    	<option <?=$slc_l;?> value="L">Laki-laki</option>
        <option <?=$slc_p;?> value="P">Perempuan</option>
    </select>
    </td>
  </tr>
  <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td><select name="tgl" id="tgl">
    	<?php
        for($tgl=1;$tgl<=31;$tgl++){
			$select_tgl = ($tgl == $tgl_pisah) ? 'selected' : '';
		?>
        <option value="<?= $tgl; ?>" <?=$select_tgl;?>>
        <?= $tgl; ?></option>
        <?php
        }
		?>
    </select>
      
      <select name="bln" id="bln">
      <?php
      	$bulan = array ('Januari','Februari','Maret','April','Mei','Juni'						,'Juli','Agustus','September','Oktober','November','Desember');
		$b=0;
		while(each($bulan)){
			if($b+1== $bln_pisah){
				$n = 'SELECTED';
			}else{
				$n = '';
			}		
	  ?>
      <option <?=$n;?> value="<?=$b+1;?>" ><?=$bulan[$b];?></option>
      <?php
      	$b++;
		}
	  ?>
      </select>
      <select name="thn" id="thn">
      <?php
		for($thn = 1990; $thn <= date('Y'); $thn++) {$select_thn = ($thn == $thn_pisah) ? 'selected' : '';	
		?>
		<option value="<?= $thn; ?>" <?=$select_thn;?>>
        <?= $thn; ?></option>
		<?php
		}
		?>
      </select>
      </td>
  </tr>
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input value="<?= $telp; ?>" type="text" name="telp" id="telp" maxlength="50"></td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea value=<?= $alamat; ?> name="alamat" id="alamat"cols="45" rows="5"><?=$alamat;?></textarea></td>
  </tr>
  <tr>
    <td>Jabatan</td>
    <td>:</td>
    <td><select name="kode_jabatan" id="kode_jabatan">
  <?php
  foreach ($data_jabatan as $data){ 
   $select_jabatan = ($data->kode_jabatan ==
	 $kode_jabatan) ? 'selected' : '';
	 ?>
	 <option value="<?= $data->kode_jabatan; ?>"
     <?= $select_jabatan; ?>><?=$data->nama_jabatan;?></option>
  <?php } ?>
    </select>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="submit" id="submit" value="Simpan">
      <input type="reset" name="reset" id="reset" value="Reset"></td>
  </tr>
  <td>&nbsp;</td>
    <td>&nbsp;</td>
   <td width="334"><a href="<?=base_url();?>karyawan/listkaryawan"><input type="button" name="button" id="button" value="Kembali Ke Menu Sebelumnya"></td>
   </td>
</table>
</form>