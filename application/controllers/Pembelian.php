<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Pembelian extends CI_Controller{
	
	public function __construct()
{
	parent:: __construct();
	$this->load->model("Pembelian_model");
	$this->load->model("Supplier_model");
	$this->load->model("Barang_model");
}
	public function index()
{
	$this->ListPembelian();
}
public function InputPembelian()
{
	$data['data_supplier']= $this->Supplier_model->tampilDataSupplier();
	if (!empty($_REQUEST)) {
		$pembelian_header = $this->Pembelian_model;
		$pembelian_header->savePembelianHeader();
		
		$id_terakhir = $pembelian_header->idTransaksiTerakhir();
		redirect("pembelian/InputPembelianDetail/" . $id_terakhir, "refresh");
		 }
	$this->load->view('input_pembelian',$data);
	}
	public function InputPembelianDetail($id_pembelian_header)
{
	$data['data_barang']= $this->Barang_model->tampilDataBarang();
	$data['id_header']= $id_pembelian_header;
	$data['data_pembelian_detail']= $this->Pembelian_model->tampilDataPembelianDetail($id_pembelian_header);
	if (!empty($_REQUEST)) {
		$this->Pembelian_model->savePembelianDetail($id_pembelian_header);
		$kode_barang = $this->input->post('kode_barang');
		$qty = $this->input->post('qty');
		$this->Barang_model->updateStok($kode_barang, $qty);
		redirect("pembelian/InputPembelianDetail/" . $id_pembelian_header, "refresh");
		 }
    $data['content'] 		= 'forms/input_pembelian_d';
	$this->load->view('input_pembelian_d',$data);
	}
	public function ListPembelian()
{
	$data['data_pembelian']= $this->Pembelian_model->tampilDataPembelian();
	$data['content'] 		    = 'forms/list_pembelian';
	$this->load->view('list_pembelian',$data);
}
	}