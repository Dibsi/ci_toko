<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Jabatan extends CI_Controller{
	
	public function __construct()
{
	parent:: __construct();
	$this->load->model("jabatan_model");
}

	public function index()
{
	$this->ListJabatan();

}

	public function ListJabatan()
{
	$data['data_jabatan']= $this->jabatan_model->tampilDataJabatan();
	$this->load->view('home1',$data);
}
	public function InputJabatan()
{
	if (!empty($_REQUEST)) {
		$m_jabatan = $this->jabatan_model;
		$m_jabatan->save();
		redirect("jabatan/index", "refresh");
	}
	$this->load->view('input1');
 }
 	public function detailjabatan($kode_jabatan)
	{
		$data['detail_jabatan'] = $this->jabatan_model->detail($kode_jabatan);
		$this->load->view('detail_jabatan', $data);
	}
	public function EditJabatan($kode_jabatan)
{
	$data['detail_jabatan'] = $this->jabatan_model->detail($kode_jabatan);
	if (!empty($_REQUEST)) {
		$m_jabatan = $this->jabatan_model;
		$m_jabatan->update($kode_jabatan);
		redirect("jabatan/index", "refresh");
	}
	$this->load->view('edit1',$data);
	}
	public function deletejabatan($kode_jabatan)
 	{
 		$m_jabatan = $this->jabatan_model;
 		$m_jabatan->delete($kode_jabatan);
 		redirect("jabatan/index", "refresh");
 	}
}



