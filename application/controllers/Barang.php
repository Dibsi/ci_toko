<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Barang extends CI_Controller{
	
	public function __construct()
{
	parent:: __construct();
	$this->load->model("barang_model");
	$this->load->model("jenis_model");
}

	public function index()
{
	$this->ListBarang();

}

	public function ListBarang()
{
	$data['data_barang']= $this->barang_model->tampilDataBarang2();
	$this->load->view('home3',$data);
}
	public function InputBarang()
{
	$data['data_jenis']= $this->jenis_model->tampilDataJenis();
	if (!empty($_REQUEST)) {
		$m_barang = $this->barang_model;
		$m_barang->save();
		redirect("barang/index", "refresh");
	}
	$this->load->view('input3',$data);
 }
 	public function detailbarang($kode_barang)
	{
		$data['detail_barang'] = $this->barang_model->detail($kode_barang);
		$this->load->view('detail_barang', $data);
	}
	public function EditBarang($kode_barang)
{
	$data['data_jenis']= $this->jenis_model->tampilDataJenis();
	$data['detail_barang'] = $this->barang_model->detail($kode_barang);
	if (!empty($_REQUEST)) {
		$m_barang = $this->barang_model;
		$m_barang->update($kode_barang);
		redirect("barang/index", "refresh");
}
	$this->load->view('edit3',$data);
 	}
 	public function deletebarang($kode_barang)
 	{
 		$m_barang = $this->barang_model;
 		$m_barang->delete($kode_barang);
 		redirect("barang/index", "refresh");
 	}
	}

