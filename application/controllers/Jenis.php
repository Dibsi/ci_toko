<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Jenis extends CI_Controller{
	
	public function __construct()
{
	parent:: __construct();
	$this->load->model("jenis_model");
}

	public function index()
{
	$this->ListJenis();

}

	public function ListJenis()
{
	$data['data_jenis']= $this->jenis_model->tampilDataJenis();
	$this->load->view('home2',$data);
}
public function InputJenis()
{
	if (!empty($_REQUEST)) {
		$m_jenis = $this->jenis_model;
		$m_jenis->save();
		redirect("jenis/index", "refresh");
	}
	$this->load->view('input2');
 }
 	public function detailjenis($kode_jenis)
	{
		$data['detail_jenis'] = $this->jenis_model->detail($kode_jenis);
		$this->load->view('detail_jenis', $data);
	}
	public function EditJenis($kode_jenis)
{
	$data['detail_jenis'] = $this->jenis_model->detail($kode_jenis);
	if (!empty($_REQUEST)) {
		$m_jenis = $this->jenis_model;
		$m_jenis->update($kode_jenis);
		redirect("jenis/index", "refresh");
}
	$this->load->view('edit2',$data);
 	}
 	public function deletejenis($kode_jenis)
 	{
 		$m_jenis = $this->jenis_model;
 		$m_jenis->delete($kode_jenis);
 		redirect("jenis/index", "refresh");
 	}
	}


