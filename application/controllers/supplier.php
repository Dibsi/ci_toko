<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Supplier extends CI_Controller{
	
	public function __construct()
{
	parent:: __construct();
	$this->load->model("supplier_model");
}

	public function index()
{
	$this->ListSupplier();

}

	public function ListSupplier()
{
	$data['data_supplier']= $this->supplier_model->tampilDataSupplier();
	$this->load->view('home4',$data);
}
	public function InputSupplier()
{
	if (!empty($_REQUEST)) {
		$m_supplier = $this->supplier_model;
		$m_supplier->save();
		redirect("supplier/index", "refresh");
	}
	$this->load->view('input4');
 }
 	public function detailsupplier($kode_supplier)
	{
		$data['detail_supplier'] = $this->supplier_model->detail($kode_supplier);
		$this->load->view('detail_supplier', $data);
	}
	public function EditSupplier($kode_supplier)
{
	$data['detail_supplier'] = $this->supplier_model->detail($kode_supplier);
	if (!empty($_REQUEST)) {
		$m_supplier = $this->supplier_model;
		$m_supplier->update($kode_supplier);
		redirect("supplier/index", "refresh");
}
	$this->load->view('edit4',$data);
 	}
 	public function deletesupplier($kode_supplier)
 	{
 		$m_supplier = $this->supplier_model;
 		$m_supplier->delete($kode_supplier);
 		redirect("supplier/index", "refresh");
 	}
	}


