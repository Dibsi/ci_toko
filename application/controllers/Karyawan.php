<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Karyawan extends CI_Controller{
	
	public function __construct()
{
	parent:: __construct();
	$this->load->model("karyawan_model");
	$this->load->model("jabatan_model");
}

	public function index()
{
	$this->ListKaryawan();

}

	public function ListKaryawan()
{
	$data['data_karyawan']= $this->karyawan_model->tampilDataKaryawan();
	$this->load->view('home',$data);
}
	public function InputKaryawan()
{
	$data['data_jabatan']= $this->jabatan_model->tampilDataJabatan();
	if (!empty($_REQUEST)) {
		$m_karyawan = $this->karyawan_model;
		$m_karyawan->save();
		redirect("karyawan/index", "refresh");
	}
	$this->load->view('input',$data);
 }
 	public function detailkaryawan($nik)
	{
		$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
		$this->load->view('detail_karyawan', $data);
	}
	public function EditKaryawan($nik)
{
	$data['data_jabatan']= $this->jabatan_model->tampilDataJabatan();
	$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
	if (!empty($_REQUEST)) {
		$m_karyawan = $this->karyawan_model;
		$m_karyawan->update($nik);
		redirect("karyawan/index", "refresh");
}
	$this->load->view('edit',$data);
 	}
 	public function deletekaryawan($nik)
 	{
 		$m_karyawan = $this->karyawan_model;
 		$m_karyawan->delete($nik);
 		redirect("karyawan/index", "refresh");
 	}
	}


